var gulp 	= require('gulp'),
	sass 	= require('gulp-sass'),
	rename 	= require('gulp-rename'),
	prefix 	= require('gulp-autoprefixer'),
	bs 		= require('browser-sync'),
	uglify	= require('gulp-uglify')

	svgstore = require('gulp-svgstore'),
	svg2string = require('gulp-svg2string'),
	svgmin = require('gulp-svgmin');

gulp.task('svg', function () {
  return gulp.src('src/svg/all/*.svg')
	.pipe(svgmin())
    .pipe(svgstore())
    .pipe(svg2string())
    .pipe(gulp.dest('src/svg'));
});

var reloadTimer;
function reloadDelay(){
	clearTimeout(reloadTimer);
	reloadTimer = setTimeout(bs.reload, 1500);
}

// CORE OPERATIONS
gulp.task('sass-main', function(){
	gulp.src('src/scss/all-styles.scss')
		.pipe(sass({
			outputStyle: 'compressed',
			includePaths: ['./src/scss/', './src/scss/styles/'],
		})
			.on('error', sass.logError))
		.pipe(prefix())
		.pipe(rename('s.min.css'))
		.pipe(gulp.dest('src/css'))
		.pipe(bs.stream());
});


// END OF CORE OPERATIONS

// WATCH TASKS
gulp.task('svg-watch', ['svg'], reloadTimer);
// END OF WATCH TASKS

// WRAPPING UP: initial compile and start watchers
gulp.task('default', ['sass-main', 'svg'], function(){
	bs({
		proxy: 'localhost:8888/2017-11-17 - KrasCvetMet/front/src'
	});


	gulp.watch(['src/scss/*.scss','src/scss/**/*.scss'], ['sass-main']);
	gulp.watch(['src/js/**/*.js', 
							'src/fonts/**/*', 
							'src/img/**/*', 
							'src/video/**/*',
							'src/*.html', 
							'src/*.php',
							'src/**/*.php'], bs.reload);
	gulp.watch(['src/svg/all/*.svg'], ['svg-watch']);
});
