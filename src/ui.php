<? include('modules/header.php'); ?>
<div class="head-block">
  <div class="head-block__menu">
    <div class="container">
      <nav>
        <ul>
          <li class="active"><a href="">Расчёт стоимости присоединения</a></li>
          <li><a href="">Мои заявки</a></li>
          <li><a href="">Сообщения<span class="quan">15</span></a></li>
          <li><a href="">Профиль</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="head-block__title">
    <div class="container"><div class="title">Пользовательское соглашение</div></div>
  </div>
</div>
<div class="container">
  <h1>UI section</h1>
  <h2>H2 Заголовок второго уровня</h2>
  <h3>H3 Заголовок третьего уровня</h3>
  <p>1.1. ООО «Цветмет» предлагает пользователю сети Интернет (далее – Пользователь) - использовать свои сервисы на условиях, изложенных в настоящем Пользовательском соглашении (далее — «Соглашение», «ПС»). Соглашение вступает в силу с момента выражения Пользователем согласия с его условиями в порядке, предусмотренном п. 1.4 Соглашения. </p>
  <p>1.2. Цветмет предлагает Пользователям доступ к широкому спектру сервисов, включая средства навигации, коммуникации, поиска, размещения и хранения разного рода информации и материалов (контента), персонализации контента, совершения покупок и т. д. Все существующие на данный момент сервисы, а также любое развитие их и/или добавление новых является предметом настоящего Соглашения.</p>

  <h3>Поля пароля</h3>
  <p>
    <span class="pass-wrap valid">
      <input type="password">
      <div class="btn-pwdswitch" placeholder="Не менее 8 символов"></div>
    </span>

    <span class="pass-wrap no-valid">
      <input type="password" aria-describedby="helpBlock">
      <div class="btn-pwdswitch"></div>
    </span>
    <span id="helpBlock" class="help-block">Не меньше 8 символов, включая цифры</span>
  </p>

  <label class="label-date"><input type="date"></label><div>
    <div class="dateRangePicker"><input type="text"></div>
  </div>
  <div>
    <div class="dateRangePickerSingle"><input type="text"></div>
  </div>
  <div>
    <div class="dateRangePicker"><input type="text"></div>
  </div>
  <div>
    <div class="dateRangePickerSingle"><input type="text"></div>
  </div>
  <div>
    <div class="dateRangePicker"><input type="text"></div>
  </div>
  <div>
    <div class="dateRangePickerSingle"><input type="text"></div>
  </div>
  <div>
    <div class="dateRangePicker"><input type="text"></div>
  </div>
  <div>
    <div class="dateRangePickerSingle"><input type="text"></div>
  </div>

  <h3>Главные кнопки</h3>
  <p>
    <button class="btn btn-sm btn-primary">Войти</button>
  </p>
  <p>
    <button class="btn btn-primary">Войти</button>
  </p>
  <p>
    <button class="btn btn-lg btn-primary">Войти</button>
  </p>
  <h3>Второстепенные кнопки</h3>
  <p>
    <button class="btn btn-sm btn-default">Зарегистрироваться</button>
  </p>
  <p>
    <button class="btn btn-default">Зарегистрироваться</button>
  </p>
  <p>
    <button class="btn btn-lg btn-default">Зарегистрироваться</button>
  </p>
  <h3>Ссылки и псевдоссылки</h3>
  <p>
    <a href="" class="">Ссылка в тексте</a>
  </p>
  <p>
    <button href="" class="pseudolink">Псевдо-ссылка второстепенная</button>
  </p>
  <p>
    <button href="" class="pseudolink-more">Расширенная псевдоссылка</button>
  </p>
  <h3>Поля</h3>
  <p>
    <select name="" id="" class="form-control chosen-select">
      <option value="1">Lorem</option>
      <option value="2">ipsum</option>
      <option value="3">dolor</option>
      <option value="4">sit</option>
      <option value="5">amet,</option>
      <option value="6">consectetur</option>
      <option value="7">adipisicing</option>
      <option value="8">elit.</option>
      <option value="9">Quibusdam</option>
      <option value="10">fuga</option>
      <option value="11">non</option>
      <option value="12">quasi</option>
      <option value="13">amet</option>
      <option value="14">fugit</option>
      <option value="15">dolorum</option>
      <option value="16">tenetur</option>
      <option value="17">pariatur</option>
      <option value="18">voluptatem</option>
      <option value="19">ea</option>
      <option value="20">iusto</option>
      <option value="21">eaque,</option>
      <option value="22">dolor</option>
      <option value="23">suscipit</option>
      <option value="24">soluta</option>
      <option value="25">illo</option>
      <option value="26">veniam</option>
      <option value="27">aut</option>
      <option value="28">mollitia</option>
      <option value="29">cum</option>
      <option value="30">veritatis</option>
    </select>
  </p>
  <p>
    <button class="btn-tooltip" data-toggle="tooltip" data-placement="top" title="<p>Рассчитано по формуле: T=C1*N</p><p>С1 – тарифная ставка (руб./кВт) на тех. присоединение.</p><p>N – объем макс. мощности (кВт)</p>">Как расчитывается цена (TOOLTIP)</button>
  </p>
  <p>
    <div class="form-group has-success">
      <label class="control-label" for="inputSuccess1">Input with success</label>
      <input type="text" class="form-control" id="inputSuccess1" aria-describedby="helpBlock2">
      <span id="helpBlock2" class="help-block">A block of help text that breaks onto a new line and may extend beyond one line.</span>
    </div>
    <div class="form-group has-warning">
      <label class="control-label" for="inputWarning1">Input with warning</label>
      <input type="text" class="form-control" id="inputWarning1">
    </div>
    <div class="form-group has-error">
      <label class="control-label" for="inputError1">Input with error</label>
      <input type="text" class="form-control" id="inputError1">
    </div>
  </p>
</div>      
<? include('modules/footer.php'); ?>