<? include('modules/head.php'); ?>
<header class="header">
  <div class="container">
    <div class="header__logo">
      <div class="img-wrap"><img src="img/logo-big.png" alt=""></div>
      <div class="text">
        Мы делаем драгоценные металлы<br>полезными и доступными людям
      </div>
    </div>
    <div class="header__auth hidden">
      <button class="btn btn-sm btn-primary js-modal-login">Войти</button>
      <button class="btn btn-sm btn-default js-modal-reg">Зарегистрироваться</button>
    </div>
    <div class="header__auth">
    <div class="user-photo">
      <img src="img/user.png" alt="Фото пользователя">
    </div>
      <button class="btn-reset user"><span class="link">Евгений Игнашов</span></button>
      <button class="btn-reset unlogin"><span class="link">Выйти</span></button>
    </div>
  </div>
</header>
<main>
