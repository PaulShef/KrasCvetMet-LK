  </main>
  <footer class="main-footer">
    <div class="footer-inner">
      <div class="container">
        <div>© ОАО «Красцветмет», 2007–2017</div>
        <div><a href="" class="link">Политика обработки персональных данных</a></div>
        <div>Проектирование и дизайн — <a href="" class="link">Игнашов.рф</a></div>
      </div>
    </div>
  </footer>
</div>


<? include('modules/all-modals.php'); ?>
<? include('modules/all-scripts.php'); ?>
</body>
</html>
