<? include('modules/header.php'); ?>

<div class="head-block">
  <div class="head-block__menu">
    <div class="container">
      <nav>
        <ul>
          <li class="active"><a href="">Расчёт стоимости присоединения</a></li>
          <li><a href="">Мои заявки</a></li>
          <li><a href="">Сообщения<span class="quan">15</span></a></li>
          <li><a href="">Профиль</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="head-block__title">
    <div class="container"><div class="title">Расчёт стоимости технологического присоединения к линии электропередач</div></div>
  </div>
  <form class="main_calc_price">
    <div class="left-block">
      <div class="row">
        <div class="col-sm-6">
          <label class="radio-styled js-tab" data-target="#tab-yur">
            <input type="radio" name="calc_price" value="1" checked> <span class="text">Юридическое лицо</span>
          </label>
        </div>
        <div class="col-sm-6">
          <label class="radio-styled js-tab" data-target="#tab-fiz">
            <input type="radio" name="calc_price" value="2"> <span class="text">Физическое лицо</span>
          </label>
        </div>
        <label>
          <div class="col-sm-6 text-right">Объём максимальной<br>требуемой мощности</div>
          <div class="col-sm-6">
            <input type="text">
            <div class="unit">кВт</div>
            <div class="info">Макс. для юр. лиц — 150 кВт</div>
          </div>
        </label>
        <label>
          <div class="col-sm-6 text-right">Расстояние до объекта<br>подключения</div>
          <div class="col-sm-6">
            <input type="text">
            <div class="unit">км</div>
            <div class="info">суммарная протяжённость воздушных и/или кабельных линий электропередачи</div>
          </div>
        </label>
      </div>
    </div>
    <div class="right-block">
      <div>Стоимость технологического присоединения</div>
      <div class="price">
        <span class="quan">120 000</span><span class="unit"> Р</span>
        <button class="btn-tooltip" data-toggle="tooltip" data-placement="top" title="<p>Рассчитано по формуле: T=C1*N</p><p>С1 – тарифная ставка (руб./кВт) на тех. присоединение.</p><p>N – объем макс. мощности (кВт)</p>">Как расчитывается цена 
          <span class="svg-wrap"><svg><use xlink:href="#tooltip"></use></svg></span>
        </button>
      </div>
      <button class="btn btn-lg btn-primary js-btn-calc-run">Оформить заявку на присоединение</button>
      <div class="text-center">Заявку можно оформить без расчёта стоимости</div>
    </div>
  </form>
</div>

<div class="container hidden-xs js-calc-container collapse">

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="tab-yur">
      <div class="card calc-requisite">
        <div class="card__title clearfix">
          Реквизиты заявителя
          <div class="pull-right small">
            Выбрано Юр. лицо <button class="pseudolink js-change-person">сменить на Физ. лицо</button>
          </div>
        </div>

        <div class="row">
          <div class="elem col-md-6 col-sm-6">ОГРНИП</div>
          <div class="elem col-md-6 col-sm-6">
            <input class="ogrnip" type="text" placeholder="000000000000000">
          </div>

          <div class="elem col-md-6 col-sm-6">Дата записи в Едином гос. реестре юр. лиц (ИП)</div>
          <div class="elem col-md-6 col-sm-6">
            <label class="label-date">
              <div class="dateRangePickerSingle"><input type="text"></div>
            </label>
          </div>

          <div class="elem col-md-6 col-sm-6">ИНН / КПП</div>
          <div class="elem col-md-6 col-sm-6">
            <input class="inn" type="text" placeholder="000000000000">
            <span class="sep text-muted">/</span>
            <input class="kpp" type="text" placeholder="000000000">
          </div>

          <div class="elem col-md-6 col-sm-6">Расчётный счёт (Р/с)</div>
          <div class="elem col-md-6 col-sm-6">
            <input class="raschet" type="text" placeholder="00000000000000000000">
          </div>

          <div class="elem col-md-6 col-sm-6">Корреспондентский счёт (К/с)</div>
          <div class="elem col-md-6 col-sm-6">
            <input class="koresp" type="text" placeholder="00000000000000000000">
          </div>

          <div class="elem col-md-6 col-sm-6">БИК</div>
          <div class="elem col-md-6 col-sm-6">
            <input class="bik" type="text" placeholder="000000000">
          </div>

          <div class="elem col-md-6 col-sm-6">Банк</div>
          <div class="elem col-md-6 col-sm-6">
            <input type="text">
          </div>

        </div>
      </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="tab-fiz">
      <div class="card calc-requisite">
        <div class="card__title clearfix">
          Реквизиты заявителя
          <div class="pull-right small">
            Выбрано Физ. лицо <button class="pseudolink js-change-person">сменить на Юр. лицо</button>
          </div>
        </div>

        <div class="row">
          <div class="elem col-md-6">Ф. И. О.</div>
          <div class="elem col-md-6">
            <input type="text">
          </div>
          <div class="elem col-md-6">Серия и номер паспорта</div>
          <div class="elem col-md-6">
            <input type="text" class="pasp-s" placeholder="0000">
            <input type="text" class="pasp-n" placeholder="000000">
          </div>
          <div class="elem col-md-6">Кем выдан паспорт</div>
          <div class="elem col-md-6">
            <input type="text">
          </div>
          <div class="elem col-md-6">Дата выдачи паспорта</div>
          <div class="elem col-md-6">
            <label class="label-date"><input type="date"></label>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="card calc-object">
    <div class="card__title">
      Объект подключения
    </div>
    <div class="row">
      <div class="elem col-md-6 col-sm-6">Ранее присоединённые мощности</div>
      <div class="elem col-md-6 col-sm-6">
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio82347" value="1"><span class="text">Не было</span>
        </label>
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio82347" value="2"><span class="text">Были</span>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6">Наименование заявителя</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Например, ТЦ «Планета»">
      </div>
      <div class="elem col-md-6 col-sm-6">Ранее присоединённая максимальная мощность</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" class="power">
        <div class="unit">кВт</div>
      </div>
      <div class="elem col-md-6 col-sm-6">Количество точек присоединения</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" class="dots">
        <div class="unit">шт</div>
      </div>
      <div class="elem col-md-6 col-sm-6">Уровень напряжения</div>
      <div class="elem col-md-6 col-sm-6">
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio8245235" value="1"><span class="text">220 В</span>
        </label>
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio8245235" value="2"><span class="text">380 В</span>
        </label>
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio8245235" value="3"><span class="text">6 кВ</span>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6">Категория надёжности энергоснабжения</div>
      <div class="elem col-md-6 col-sm-6">
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio8123" value="1"><span class="text">1</span>
        </label>
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio8123" value="2"><span class="text">2</span>
        </label>
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio8123" value="3"><span class="text">3</span>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6 pad-top-null">В какие сроки вам необходима электроэнергия?</div>
      <div class="elem col-md-6 col-sm-6 js-date-format-toggle">
        <select class="form-control date">
          <option value="1">точная дата</option>
          <option value="2">неточная дата</option>
        </select>
        <div class="dateRangePickerSingle-dft"><input type="text"></div>
      </div>

      <div class="elem col-md-6 col-sm-6 pad-top-null">Заявленный характер нагрузки
        <span class="text-muted">(вид экономической деятельности хозяйствующего субъекта)</span>
      </div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Например, торговый центр">
      </div>

      <div class="elem col-md-6 col-sm-6 pad-top-null">Энергопринимающие устройства, планируемые к присоединению</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Например, трансформаторная подстанция">
      </div>

      <div class="elem col-md-6 col-sm-6 text-right">Максимальная мощность</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" class="power">
        <div class="unit">кВт</div>
        <div class="info">Макс. для физ. лиц — 150 кВт</div>
      </div>

      <div class="elem col-md-6 col-sm-6">Порядок рассчётов и условия рассрочки оплаты</div>
      <div class="elem col-md-6 col-sm-6">
        <div>
          <label class="radio-styled">
            <input type="radio" name="radio132476" value="1"><span class="text">100%</span>
          </label>
        </div>
        <div>
          <label class="radio-styled">
            <input type="radio" name="radio132476" value="2"><span class="text">95% оплаты на период до 3-х лет</span>
          </label>
        </div>
        <div>
          <label class="radio-styled">
            <input type="radio" name="radio132476" value="3"><span class="text">15–30–45–10%</span>
          </label>
        </div>
      </div>
    </div>
  </div>

  <div class="card calc-deadline">
    <div class="card__title">
      Сроки проектирования и введения в эксплуатацию энергопринимающего устройства <br>
      <div class="text-muted">(в том числе по этапам и очередям)</div>
    </div>
    <div class="row">
      <div class="elem col-md-6 col-sm-6">Дата введения в эксплуатацию</div>
      <div class="elem col-md-3">
        <label class="label-date">
          <div class="dateRangePickerSingle"><input type="text"></div>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6">Мощность</div>
      <div class="elem col-md-3">
        <input type="text" class="power">
        <div class="unit">кВт</div>
      </div>
      <div class="stage">Этап <span class="num">1</span></div>
    </div>
    <div class="row">
      <div class="elem col-md-6 col-sm-6">Дата введения в эксплуатацию</div>
      <div class="elem col-md-3">
        <label class="label-date">
          <div class="dateRangePickerSingle"><input type="text"></div>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6">Мощность</div>
      <div class="elem col-md-3">
        <input type="text" class="power">
        <div class="unit">кВт</div>
      </div>
      <div class="stage">
        Этап <span class="num">2</span>
        <button class="js-remove-stage btn-reset">
          <span class="svg-wrap"><svg><use xlink:href="#remove"></use></svg></span>
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-6">
        <button class="js-add-stage pseudolink">
          <span class="svg-wrap"><svg><use xlink:href="#plus"></use></svg></span>
          <span class="text">добавить ещё этап</span>
        </button> 
      </div>
    </div>
  </div>

  <div class="card calc-deadline">
    <div class="card__title">
      Планируемое распределение макс. мощности, сроков ввода и категория надёжности электроснабжения при вводе энергопринимающих устр-в по этапам и очередям
    </div>
    <div class="row">
      <div class="elem col-md-6 col-sm-6">Мощность</div>
      <div class="elem col-md-3">
        <input type="text" class="power">
        <div class="unit">кВт</div>
      </div>
      <div class="elem col-md-6 col-sm-6">Категория</div>
      <div class="elem col-md-4">
        <label class="radio-styled radio-inline">
          <input type="radio" name="radio5934687" value="1"><span class="text">1</span>
        </label>
        <label class="radio-styled radio-inline">
          <input type="radio" name="radio5934687" value="2"><span class="text">2</span>
        </label>
        <label class="radio-styled radio-inline">
          <input type="radio" name="radio5934687" value="3"><span class="text">3</span>
        </label>
      </div>
      <div class="stage">Этап <span class="num">1</span></div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-6">
        <button class="js-add-stage pseudolink">
          <span class="svg-wrap"><svg><use xlink:href="#plus"></use></svg></span>
          <span class="text">добавить ещё этап</span>
        </button> 
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card__title">
      Адреса
    </div>
    <div class="row">
      <div class="elem col-md-6 col-sm-6"><strong>Адрес подключения</strong></div>
      <div class="elem col-md-6 col-sm-6"></div>
      <div class="elem col-md-6 col-sm-6">Населённый пункт</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Начните писать название" value="Красноярск" disabled>
      </div>
      <div class="elem col-md-6 col-sm-6">Улица</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Начните писать название">
      </div>
      <div class="elem col-md-6 col-sm-6">Адрес</div>
      <div class="elem col-md-6 col-sm-6">
        <div class="row">
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Дом</div>
          </div>
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Корпус</div>
          </div>
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Квартира</div>
          </div>
        </div>  
        <button class="pseudolink js-textarea-toggle">добавить примечание</button>

        <div class="textarea-wrap collapse">
          <textarea class="form-control" rows="3"></textarea>
        </div>
      </div>
      <div class="elem col-md-6 col-sm-6"><strong>Юридический адрес</strong></div>
      <div class="elem col-md-6 col-sm-6">
        <label class="check-styled">
          <input type="checkbox" name="check17625" value="1">
          <span class="text">
            Совпадает с адресом подключения
          </span>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6"><strong>Почтовый адрес</strong></div>
      <div class="elem col-md-6 col-sm-6">
        <label class="check-styled">
          <input type="checkbox" name="check17641235" value="1">
          <span class="text">
            Совпадает с адресом подключения
          </span>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6">Населённый пункт</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Начните писать название">
      </div>
      <div class="elem col-md-6 col-sm-6">Улица</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Начните писать название">
      </div>
      <div class="elem col-md-6 col-sm-6">Адрес</div>
      <div class="elem col-md-6 col-sm-6">
        <div class="row">
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Дом</div>
          </div>
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Корпус</div>
          </div>
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Квартира</div>
          </div>
        </div>  
        <button class="pseudolink js-textarea-toggle">добавить примечание</button>

        <div class="textarea-wrap collapse">
          <textarea class="form-control" rows="3"></textarea>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card__title">
      Контакты
    </div>
    <div class="row">
      <div class="elem col-md-6 col-sm-6">Телефон</div>
      <div class="elem col-md-6 col-sm-6"><input class="input-tel" type="tel" placeholder="+7 (___) ___-__-__"></div>
      <div class="elem col-md-6 col-sm-6">Эл. почта</div>
      <div class="elem col-md-6 col-sm-6"><input type="text"></div>
    </div>
  </div>



  <div class="card">
    <div class="card__title">
      Документы
    </div>
    <div class="row">
      <div class="elem col-md-6 col-sm-6">План расположения энергопрининимающего устройства,  присоединяемого к сетям сетевой организации.<br>
        <div class="text-muted">(Нарисуйте максимально понятно улицу, ваш участок, ваше сооружение и место, с которого вам хотелось бы получать электроэнергию)</div>
      </div>
      <div class="elem col-md-6 col-sm-6">
        <div class="dropzone-wrap"> <!-- used DROPZONE.js -->
          <form action="/actions" class="dropzone"></form>
        </div>
      </div>
      <div class="elem col-md-6 col-sm-6">Однолинейная схема электросетей, присоединяемых к электросетям сетевой организации, номинальный класс напряжения которых составляет 35 кВ и выше, с указанием возможности резервирования от собственных источников энергоснабжения (включая резервирование для собственных нужд) и возможности переключения нагрузок (генерации) по внутренним сетям заявителя</div>
      <div class="elem col-md-6 col-sm-6">
        <div class="dropzone-wrap"> <!-- used DROPZONE.js -->
          <form action="/actions" class="dropzone"></form>
        </div>
      </div>
      <div class="elem col-md-6 col-sm-6">Перечень и мощность энергопринимающих устройств, которые могут быть присоединены к устройствам противоаварийной автоматике</div>
      <div class="elem col-md-6 col-sm-6">
        <div class="dropzone-wrap"> <!-- used DROPZONE.js -->
          <form action="/actions" class="dropzone"></form>
        </div>
      </div>
      <div class="elem col-md-6 col-sm-6">Копия документа, подтверждающего право собственности или иное законное основание на объект капитального строительства
        <button class="btn-tooltip" data-toggle="tooltip" data-placement="top" title="Всплывающая подсказка"><span class="svg-wrap"><svg><use xlink:href="#tooltip"></use></svg></span></button>
      </div>
      <div class="elem col-md-6 col-sm-6">
        <div class="dropzone-wrap"> <!-- used DROPZONE.js -->
          <form action="/actions" class="dropzone"></form>
        </div>
      </div>
      <div class="elem col-md-6 col-sm-6">Доверенность или иные документы, подтверждающие полномочия представителя заявителя, подающего и получающего документы, в случае подачи заявки представителем заявителя</div>
      <div class="elem col-md-6 col-sm-6">
        <div class="dropzone-wrap"> <!-- used DROPZONE.js -->
          <form action="/actions" class="dropzone"></form>
        </div>
      </div>
      <div class="elem col-md-6 col-sm-6">Любые дополнительные файлы, которые вы считаете нужными</div>
      <div class="elem col-md-6 col-sm-6">
        <div class="dropzone-wrap"> <!-- used DROPZONE.js -->
          <form action="/actions" class="dropzone"></form>
        </div>
      </div>
    </div>
  </div>


  <div class="card">
    <div class="card__title">
      Намериваюсь заключить договор купли-продажи (поставки) электрической энергии <span class="text-muted">(в этом случае вам нужно заключить и договор на передачу электроэнергии с ОАО «Красцветмет»)</span>
    </div>
    <div class="row">

      <div class="elem col-md-6 col-sm-6">Намереваюсь заключить договор купли-продажи (поставки) электрической энергии</div>
      <div class="elem col-md-6 col-sm-6">
        <label class="radio-styled radio-inline">
          <input type="radio" name="radio585287" value="1"><span class="text">Да</span>
        </label>
        <label class="radio-styled radio-inline">
          <input type="radio" name="radio585287" value="2"><span class="text">Нет</span>
        </label>
      </div>

      <div class="elem col-md-6 col-sm-6 pad-top-null">Наименование энергосбытовой организации, с которой намереваетесь заключить договор</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Начните писать название">
      </div>

      <div class="elem col-md-6 col-sm-6">Способ передачи проекта договора и технологических условий</div>
      <div class="elem col-md-6 col-sm-6">
        <label class="radio-styled">
          <input type="radio" name="radio2137" value="1">
          <span class="text">
            Заберу сам<br><span class="text-muted">
            Красноярск, Транспортный проезд 1, оф. 201.<br>
            <strong>Предварительно запишитесь по тел.
              +7 902 910 3486</strong>
            </span>
          </span>
        </label>
        <label class="radio-styled">
          <input type="radio" name="radio2137" value="2">
          <span class="text">
            Отправьте почтой России<br>
            <span class="text-muted">Красноярск, пер. Тихий, д. 22, кв. 502.</span>
          </span>
        </label>
        <label class="radio-styled">
          <input type="radio" name="radio2137" value="3">
          <span class="text">
            Отправьте почтой России на другой адрес
          </span>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6">Населённый пункт</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Начните писать название">
      </div>
      <div class="elem col-md-6 col-sm-6">Улица</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" placeholder="Начните писать название">
      </div>
      <div class="elem col-md-6 col-sm-6">Адрес</div>
      <div class="elem col-md-6 col-sm-6">
        <div class="row">
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Дом</div>
          </div>
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Корпус</div>
          </div>
          <div class="col-xs-4">
            <input type="text">
            <div class="info">Квартира</div>
          </div>
        </div>  
        <button class="pseudolink js-textarea-toggle">добавить примечание</button>

        <div class="textarea-wrap collapse">
          <textarea class="form-control" rows="3"></textarea>
        </div>
      </div>
    </div>
  </div>

  <div class="calc-controls">
    <div>
      <button class="btn btn-primary btn-lg">Отправить заявку на присоединение</button>
      <div class="info">
        Отправляя заявку, даю согласие на обработку данных согласно <a href="" class="link"><span class="text">политике обработки персональных данных</span></a>
      </div>
    </div>
    <div>
      <button class="btn btn-default btn-lg">Сохранить и продолжить позже</button>
      <div class="info">
        Потребуется авторизация или регистрация
      </div>
    </div>
  </div>

</div>

<? include('modules/footer.php'); ?>