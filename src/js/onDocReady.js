function onDocReady(){

  console.log('onDocReady START');

  $("input[type='tel']").mask("+7(999) 999-9999");
  

  $(".chosen-select").chosen();

  if ($('.js-date-format-toggle').length != 0) {
    $('.js-date-format-toggle .dateRangePickerSingle-dft input').dateRangePicker({
          language: 'ru',
          startOfWeek: 'monday',
          monthSelect: true,
          yearSelect: [1950, 2050],
          separator : ' — ',
          singleDate : true,
          format: 'D MMM YYYY'
        });

    $(".js-date-format-toggle select").chosen().on('change', function(evt, params) {
      console.log(params.selected);
      if (params.selected == 1) {

        var $R = $(".js-date-format-toggle");
        $R.find('.dateRangePicker-dft').remove();
        $R.append('<div class="dateRangePickerSingle-dft"><input type="text"></div>')
        $R.find('.dateRangePickerSingle-dft input').dateRangePicker({
          language: 'ru',
          startOfWeek: 'monday',
          monthSelect: true,
          yearSelect: [1950, 2050],
          separator : ' — ',
          singleDate : true,
          format: 'D MMM YYYY'
        });
      } else if (params.selected == 2) {

        var $R = $(".js-date-format-toggle");
        $R.find('.dateRangePickerSingle-dft').remove();
        $R.append('<div class="dateRangePicker-dft"><input type="text"></div>')
        $R.find('.dateRangePicker-dft input').dateRangePicker({
          language: 'ru',
          startOfWeek: 'monday',
          monthSelect: true,
          yearSelect: [1950, 2050],
          separator : ' — ',
          format: 'D MMM YYYY'
        });
      }
    });
  }

  


  $('[data-toggle="tooltip"]').tooltip();

  $('.order .js-btn-order-docs').click(function () {
    var $R = $(this).next();
    if (!$R.hasClass('collapsing')) {
      $R.collapse('toggle');
      $(this).closest('.docs').toggleClass('open');
    } 
  })

  $('[data-toggle="popover"]').popover({'html': true});

  $('.js-btn-change').click(function () {
    $(this).addClass('hidden').closest('.card__title').nextAll('.row').removeClass('disabled').find('input').removeAttr('disabled');
    $(this).closest('.card').find('.change-block').collapse('show');
  })


  $('.js-btn-cancel-change').click(function () {
    $(this).closest('.change-block').collapse('hide')
    .prevAll('.row').addClass('.disabled')
    .find('input').attr('disabled');
    $(this).closest('.card').find('.js-btn-change').removeClass('hidden');
  });


  $('.js-modal-login').click(function () {
    $('#login').modal('show')
    .find('#sign_in').addClass('active')
    .nextAll('#sign_up').removeClass('active');
  });


  $('.js-modal-reg').click(function () {
    $('#login').modal('show')
    .find('#sign_in').removeClass('active')
    .nextAll('#sign_up').addClass('active');
  });

  $('.js-remove-stage').click(function () {

    var $R = $(this).closest('.card');
    $(this).closest('.row').remove();
    $R.children('.row').each(function ($i) {
      $(this).find('.stage .num').html($i + 1);
    });
  });

  $('.js-add-stage').click(function () {
    var $num = $(this).closest('.card').children('.row').length,
    $html =
    '<div class="row">'
    +'<div class="elem col-md-6 col-sm-6">Дата введения в эксплуатацию</div>'
    +'<div class="elem col-md-3">'
    +'<div class="dateRangePickerSingle"><input type="text"></div>'
    +'</div>'
    +'<div class="elem col-md-6 col-sm-6">Мощность</div>'
    +'<div class="elem col-md-3">'
    +'<input type="text" class="power">'
    +'<div class="unit">кВт</div>'
    +'</div>'
    +'<div class="stage">'
    +'Этап <span class="num">' + $num + '</span>'
    +'<button class="js-remove-stage btn-reset">'
    +'<span class="svg-wrap"><svg><use xlink:href="#remove"></use></svg></span>'
    +'</button>'
    +'</div>'
    +'</div>';
    var $R = $(this).closest('.card');
    $(this).closest('.row').before($html);
    $R.children('.row').eq($num - 1)
    .find('.js-remove-stage').click(function () {

      $(this).closest('.row').remove();
      $R.children('.row').each(function ($i) {
        $(this).find('.stage .num').html($i + 1);
      });
    });
    $R.children('.row').eq($num - 1).find('.dateRangePickerSingle input').dateRangePicker({
      language: 'ru',
      startOfWeek: 'monday',
      monthSelect: true,
      yearSelect: [1950, 2050],
      separator : ' — ',
      singleDate : true,
      format: 'D MMM YYYY'
    });

  });


  $('.btn-pwdswitch').click(function () {
    var field = $(this).prev();
    if (field.attr('type') == 'text'){
      field.get(0).type = 'password';
      $(this).removeClass('open');
      return false;
    }
    if (field.attr('type') == 'password'){
      field.get(0).type = 'text';
      $(this).addClass('open');
      return false;
    }
    return false;
  });


  $('.js-change-person').click(function () {
    var $T = $(this),
    $R = $('.main_calc_price'),
    $val = $R.find('[name="calc_price"]:checked').val();
    $('html,body').stop().animate({ scrollTop: $($R).offset().top }, 1000);
    if ($val == 1) {
      $T.text('сменить на Юр. лицо').prev().text('Выбрано Физ. лицо ');
      $R.find('input[value="2"]').click();
    } else if ($val == 2) {
      $T.text('сменить на Физ. лицо').prev().text('Выбрано Юр. лицо ');
      $R.find('input[value="1"]').click();
    }
    'Выбрано Юр. лицо <button class="pseudolink js-change-person">сменить на Физ. лицо</button>'
  })

  $('.cities-nav a').on('click', function(e) {
    var $R = $(this).attr('href');
    $('html,body').stop().animate({ scrollTop: $($R).offset().top }, 1000);
    e.preventDefault();
  });

  // отключение поля в модалке регистрации при изменении типа лица
  $('#sign_up .radio-styled input').click(function () {
    if ($(this).val() == 1) {
      $('.js-org-hid').show()
    } else if ($(this).val() == 2) {
      $('.js-org-hid').hide()
    }
  })


  $('.js-btn-calc-run').click(function (e) {
    $('.js-calc-container').collapse('show');
    e.preventDefault()
    $(".js-calc-container .chosen-select, .js-date-format-toggle select").chosen('destroy').chosen("reload");
  })

  // "добавить примечание"
  $('.js-textarea-toggle').click(function () {
    $(this).next().collapse('toggle')
  })

  $('.tab-pane').tab();
  // переключение табов по классу
  $('.js-tab').click(function () {
    $(this).tab('show');
  });

  var $dtrs = $('[class~="dateRangePicker"] input');

  if ($dtrs.length != 0) {

    for (var i = $dtrs.length - 1; i >= 0; i--) {
      $dtrs.eq(i).dateRangePicker({
        language: 'ru',
        startOfWeek: 'monday',
        monthSelect: true,
        yearSelect: [1950, 2050],
        separator : ' — ',
        format: 'D MMM YYYY'
      });
    }
  }

  var $dtrs = $('[class~="dateRangePickerSingle"] input');

  if ($dtrs.length != 0) {

    for (var i = $dtrs.length - 1; i >= 0; i--) {
      $dtrs.eq(i).dateRangePicker({
        language: 'ru',
        startOfWeek: 'monday',
        monthSelect: true,
        yearSelect: [1950, 2050],
        separator : ' — ',
        singleDate : true,
        format: 'D MMM YYYY'
      });
    }
  }

  console.log('onDocReady END');
}

jQuery(document).ready(onDocReady);