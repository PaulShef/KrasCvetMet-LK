<? include('modules/header.php'); ?>
<div class="head-block">
  <div class="head-block__menu">
    <div class="container">
      <nav>
        <ul>
          <li class="active"><a href="">Расчёт стоимости присоединения</a></li>
          <li><a href="">Мои заявки</a></li>
          <li><a href="">Сообщения<span class="quan">15</span></a></li>
          <li><a href="">Профиль</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="head-block__title">
    <div class="container"><div class="title">Личный кабинет</div></div>
  </div>
</div>
<div class="container">

  <div class="card">
    <div class="card__title">
      Контакты и пароль
      <div class="pull-right small">
        <button class="pseudolink js-btn-change">изменить</button>
      </div>
    </div>
    
    <div class="row disabled">
      <div class="elem col-md-6 col-sm-6">Телефон</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="tel" placeholder="+x (xxx) xxx-xx-xx" value="+7 (902) 910-34-86">
      </div>
      
      <div class="elem col-md-6 col-sm-6">Эл. почта</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="text" disabled value="evgenii.ignashov@mail.ru">
      </div>
      
      <div class="elem col-md-6 col-sm-6">Пароль</div>
      <div class="elem col-md-6 col-sm-6">
        <input type="password" disabled value="**********">
      </div>
      
    </div>

    <div class="change-block collapse">
      <div class="row">

        <div class="elem col-md-6 col-sm-6">Старый ароль</div>
        <div class="elem col-md-6 col-sm-6">
          <span class="pass-wrap">
            <input type="password">
            <div class="btn-pwdswitch" placeholder="Не менее 8 символов"></div>
          </span>
        </div>

        <div class="elem col-md-6 col-sm-6">Новый пароль</div>
        <div class="elem col-md-6 col-sm-6">
          <span class="pass-wrap">
            <input type="password" aria-describedby="helpBlock">
            <div class="btn-pwdswitch"></div>
          </span>
          <span id="helpBlock" class="help-block">Не меньше 8 символов, включая цифры</span>
        </div>

        <div class="elem col-md-6 col-sm-6"></div>
        <div class="elem col-md-6 col-sm-6">
          <button class="btn btn-primary">Сохранить изменения</button>
          <button class="btn btn-default js-btn-cancel-change">Отменить</button>
        </div>
      </div>
    </div>
  </div>


  <div class="card">
    <div class="card__title">
      Адреса
      <div class="pull-right small">
        <button class="pseudolink js-btn-change">изменить</button>
      </div>
    </div>
    <div class="row disabled">
      <div class="elem col-md-6 col-sm-6"><strong>Адрес подключения</strong></div>
      <div class="elem col-md-6 col-sm-6"></div>
      <div class="elem col-md-6 col-sm-6">Наименование заявителя</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled type="text" placeholder="Начните писать название" value="ИП Игнашов Евгений Константинович">
      </div>
      <div class="elem col-md-6 col-sm-6">Населённый пункт</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled type="text" placeholder="Начните писать название" value="Красноярск">
      </div>
      <div class="elem col-md-6 col-sm-6">Улица</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled type="text" placeholder="Начните писать название" value="пер. Тихий">
      </div>
      <div class="elem col-md-6 col-sm-6">Адрес</div>
      <div class="elem col-md-6 col-sm-6">
        <div class="row">
          <div class="col-xs-4">
            <input disabled type="text" value="22" placeholder="--">
            <div class="info">Дом</div>
          </div>
          <div class="col-xs-4">
            <input disabled type="text" placeholder="--">
            <div class="info">Корпус</div>
          </div>
          <div class="col-xs-4">
            <input disabled type="text" value="502" placeholder="--">
            <div class="info">Квартира</div>
          </div>
        </div>  
        <button class="pseudolink">добавить примечание</button><br><br>
        <textarea class="form-control hidden" rows="3"></textarea>
      </div>
      <div class="elem col-md-6 col-sm-6"><strong>Юридический адрес</strong></div>
      <div class="elem col-md-6 col-sm-6">
        <label class="check-styled">
          <input disabled type="checkbox" name="check17625" value="1">
          <span class="text">
            Совпадает с адресом подключения
          </span>
        </label>
      </div>
      <div class="elem col-md-6 col-sm-6"><strong>Почтовый адрес</strong></div>
      <div class="elem col-md-6 col-sm-6">
        <label class="check-styled">
          <input disabled type="checkbox" name="check17641235" value="1">
          <span class="text">
            Совпадает с адресом подключения
          </span>
        </label>
      </div>
    </div>

    <div class="change-block collapse">
      <div class="row">
        <div class="elem col-md-6 col-sm-6"></div>
        <div class="elem col-md-6 col-sm-6">
          <button class="btn btn-primary">Сохранить изменения</button>
          <button class="btn btn-default js-btn-cancel-change">Отменить</button>
        </div>
      </div>
    </div>
  </div>


  <div class="card calc-requisite">
    <div class="card__title">
      Реквизиты
      <div class="pull-right small">
        <button class="pseudolink js-btn-change">изменить</button>
      </div>
    </div>
    
    <div class="row disabled">
      <div class="elem col-md-6 col-sm-6">ОГРНИП</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled="" class="ogrnip" type="text" placeholder="000000000000000" value="316246800104767">
      </div>
      
      <div class="elem col-md-6 col-sm-6">Дата записи в Едином гос. реестре юр. лиц (ИП)</div>
      <div class="elem col-md-6 col-sm-6">
        <label class="label-date"><input disabled="" type="date" value="2016-06-20"></label>
      </div>
      
      <div class="elem col-md-6 col-sm-6">ИНН / КПП</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled="" class="inn" type="text" placeholder="000000000000" value="246210135951">
        <span class="sep text-muted">/</span>
        <input disabled="" class="kpp" type="text" placeholder="000000000" value="123456070">
      </div>
      
      <div class="elem col-md-6 col-sm-6">Расчётный счёт (Р/с)</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled="" class="raschet" type="text" placeholder="00000000000000000000" value="40802810321510000500">
      </div>
      
      <div class="elem col-md-6 col-sm-6">Корреспондентский счёт (К/с)</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled="" class="koresp" type="text" placeholder="00000000000000000000" value="30101810350040000864">
      </div>
      
      <div class="elem col-md-6 col-sm-6">БИК</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled="" class="bik" type="text" placeholder="000000000" value="045004864">
      </div>
      
      <div class="elem col-md-6 col-sm-6">Банк</div>
      <div class="elem col-md-6 col-sm-6">
        <input disabled="" type="text" value='СИБИРСКИЙ ФИЛИАЛ АО КБ "МОДУЛЬБАН"'>
      </div>
      
    </div>

    <div class="change-block collapse">
      <div class="row">
        <div class="elem col-md-6 col-sm-6"></div>
        <div class="elem col-md-6 col-sm-6">
          <button class="btn btn-primary">Сохранить изменения</button>
          <button class="btn btn-default js-btn-cancel-change">Отменить</button>
        </div>
      </div>
    </div>
  </div>

</div>
<? include('modules/footer.php'); ?>