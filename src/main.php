<? include('modules/header.php'); ?>
<div class="head-block">
  <div class="head-block__title">
    <div class="container"><div class="title">Расчёт стоимости технологического присоединения к линии электропередач</div></div>
  </div>
  <form class="main_calc_price">
    <div class="left-block">
      <div class="row">
        <div class="col-sm-6">
          <label class="radio-styled">
            <input type="radio" name="calc_price" value="1" checked> <span class="text">Юридическое лицо</span>
          </label>
        </div>
        <div class="col-sm-6">
          <label class="radio-styled">
            <input type="radio" name="calc_price" value="2"> <span class="text">Физическое лицо</span>
          </label>
        </div>
        <label>
          <div class="col-sm-6 text-right">Объём максимальной<br>требуемой мощности</div>
          <div class="col-sm-6">
            <input type="text">
            <div class="unit">кВт</div>
            <div class="info">Макс. для юр. лиц — 150 кВт</div>
          </div>
        </label>
        <label>
          <div class="col-sm-6 text-right">Расстояние до объекта<br>подключения</div>
          <div class="col-sm-6">
            <input type="text">
            <div class="unit">км</div>
            <div class="info">суммарная протяжённость воздушных и/или кабельных линий электропередачи</div>
          </div>
        </label>
      </div>
    </div>
    <div class="right-block">
      <div>Стоимость технологического присоединения</div>
      <div class="price"><span class="quan">120 000</span><span class="unit"> &#8399;</span></div>
      <button class="btn btn-lg btn-primary">Оформить заявку на присоединение</button>
      <div class="text-center">Заявку можно оформить без расчёта стоимости</div>
    </div>
  </form>
  <!-- <div class="head-block__calc"></div> -->
</div>

<div class="container">
  <div class="card main-requisite">
    <div class="card__title">
      Реквизиты заявителя
      <div class="pull-right small">
        <span>Выбрано Юр. лицо </span><button class="pseudolink js-change-person">сменить на Физ. лицо</button>
      </div>
    </div>
    <div class="row">
      <div class="elem col-md-6">Ф. И. О.</div>
      <div class="elem col-md-6">
        <input type="text">
      </div>
      <div class="elem col-md-6">Серия и номер паспорта</div>
      <div class="elem col-md-6">
        <input type="text" class="pasp-s" placeholder="0000">
        <input type="text" class="pasp-n" placeholder="000000">
      </div>
      <div class="elem col-md-6">Кем выдан паспорт</div>
      <div class="elem col-md-6">
        <input type="text">
      </div>
      <div class="elem col-md-6">Дата выдачи паспорта</div>
      <div class="elem col-md-6">
        <label class="label-date"><div class="dateRangePickerSingle"><input type="text"></div></label>
      </div>
    </div>
  </div>

  <div class="card main-object">
    <div class="card__title">
      Объект подключения
    </div>
    <div class="row">
      <div class="elem col-md-6">Ранее присоединённые мощности</div>
      <div class="elem col-md-6">
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio82347" value="1"><span class="text">Не было</span>
        </label>
        <label class="radio-inline radio-styled">
          <input type="radio" name="radio82347" value="2"><span class="text">Были</span>
        </label>
      </div>
      <div class="elem col-md-6">Наименование заявителя</div>
      <div class="elem col-md-6">
        <input type="text" placeholder="Например, ТЦ «Планета»">
      </div>
    </div>
  </div>

  <div class="card calc-deadline">
    <div class="card__title">
      Сроки проектирования и введения в эксплуатацию энергопринимающего устройства <br>
      <div class="text-muted">(в том числе по этапам и очередям)</div>
    </div>
    <div class="row">
      <div class="elem col-md-6">Дата введения в эксплуатацию</div>
      <div class="elem col-md-3">
        <label class="label-date"><div class="dateRangePickerSingle"><input type="text"></div></label>
      </div>
      <div class="elem col-md-6">Мощность</div>
      <div class="elem col-md-3">
        <input type="text" class="power">
        <div class="unit">кВт</div>
      </div>
      <div class="stage">Этап <span class="num">1</span></div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-6">
        <button class="js-add-stage pseudolink">
          <div class="svg-wrap">
            <svg>
              <use xlink:href="#plus-circle"></use>
            </svg>
          </div>
          <span class="text">добавить ещё этап</span>
        </button> 
      </div>
    </div>
  </div>
</div>
<? include('modules/footer.php'); ?>